#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ncurses.h>

#define FIELD_VISIBLE -1
#define FIELD_HIDDEN -2
#define FIELD_BOMB -3
#define CURSOR '_'
#define HIDDEN_FIELD_LABEL 'X'

using namespace std;

void printSheet(int **, int, int, int, int);

int main(int argc, char *argv[]) {
    bool 
        gameOver = false;
    int 
        rows, cols, bombs, // user defined
        row = 0, col = 0,
        bombsSet = 0,
        key,
        i, j;

    cout << "Rows: ";
    cin >> rows;
    cout << "Cols: ";
    cin >> cols;
    cout << "Bombs: ";
    cin >> bombs;

    srand(time(NULL));

    int **bombsArr = new int*[rows];
    int **playSheet = new int*[rows];

    // Fill in two similar array with -2
    // It signify that field is hidden
    for (int i = 0; i < rows; i++) {
        bombsArr[i] = new int[cols];
        playSheet[i] = new int[cols];
        for (int j = 0; j < cols; j++) {
            bombsArr[i][j] = playSheet[i][j] = FIELD_HIDDEN;
        }
    }

    // Set bombs on the hidden array
    // bombsArr is used to compare values
    while (bombsSet < bombs) {
        do {
            i = rand() % rows;
            j = rand() % cols;
        } while (bombsArr[i][j] != FIELD_HIDDEN);

        bombsArr[i][j] = FIELD_BOMB;
        bombsSet++;
    }

    // Curses Initializations
    initscr();
    keypad(stdscr, TRUE);
    noecho();

    while ((key = getch()) != '#' && !gameOver) {
        clear();
        refresh();
        if (key == '\n') {
            if (bombsArr[row][col] != FIELD_BOMB) {
                int cBombs = 0;
                if (row - 1 != -1) {
                    for( int i = -1; i <= 1; ++i ) {
                        if (bombsArr[row - 1][col + i] == FIELD_BOMB) {
                            ++cBombs;
                        }
                    }
                }
                if (bombsArr[row][col - 1] == FIELD_BOMB) {
                    ++cBombs;
                }
                if (bombsArr[row][col + 1] == FIELD_BOMB) {
                    ++cBombs;
                }
                if (row + 1 != rows) {
                    for( int i = -1; i <= 1; ++i ) {
                        if (bombsArr[row + 1][col + i] == FIELD_BOMB) {
                            ++cBombs;
                        }
                    }
                }
                // Store numbers of bombs around current field
                playSheet[row][col] = cBombs;
                // Mark field as exposed
                bombsArr[row][col] = FIELD_VISIBLE;

                // Check if use win the game
                int cHidden = 0;
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < cols; j++) {
                        if (playSheet[i][j] == FIELD_HIDDEN) {
                            ++cHidden;
                        }
                    }
                }
                if (cHidden == cBombs) {
                    cout << "WINNER! \n";
                    gameOver = true;
                }
                printSheet(playSheet, rows, cols, row, col);
            } else {
                cout << "Game Over! \n";
                gameOver = true;
            }
        }
        switch (key) {
            case KEY_UP:
                --row;
                if (row < 0) {
                    row = rows - 1;
                }
                printSheet(playSheet, rows, cols, row, col);
                break;
            case KEY_DOWN:
                ++row;
                if (row == rows) {
                    row = 0;
                }
                printSheet(playSheet, rows, cols, row, col);
                break;
            case KEY_LEFT:
                --col;
                if (col < 0) {
                    col = cols - 1;
                }
                printSheet(playSheet, rows, cols, row, col);
                break;
            case KEY_RIGHT:
                ++col;
                if (col == cols) {
                    col = 0;
                }
                printSheet(playSheet, rows, cols, row, col);
                break;
        }
        refresh();
    }

    return 0;
}

/**
 * @param array Two dimensions array with the field of game 
 * @param numRows Height of play sheet
 * @param numColsHeight of play sheet
 * @param cursorRow Current cursor y position
 * @param cursorCol Current cursor x position
 */
void printSheet(int ** array, int numRows, int numCols, int cursorRow = 0, int cursorCol = 0) {
    int i, j;
    cout << "\n\r";
    for (i = 0; i < numRows; i++) {
        for (j = 0; j < numCols; j++) {
            if (i == cursorRow && j == cursorCol) {
                cout << setw(4) << CURSOR;
            } else {
                if (array[i][j] == FIELD_HIDDEN) {
                    cout << setw(4) << HIDDEN_FIELD_LABEL;
                } else {
                    cout << setw(4) << (int) array[i][j];
                }
            }
        }
        cout << "\n\r";
    }
}
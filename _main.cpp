#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ncurses.h>
#include <unistd.h>

using namespace std;

void printDebug(int ** x, int num_rows, int num_cols) {
    int i, j;
    for (i = 0; i < num_rows; i++) {
        for (j = 0; j < num_cols; j++) {
            cout << x[i][j] << " ";
        }
        cout << "\n";
    }
}

void printSheet(int ** x, int num_rows, int num_cols) {
    int i, j;
    for (i = 0; i < num_rows; i++) {
        for (j = 0; j < num_cols; j++) {
            if (x[i][j] == 0) {
                cout << setw(4) << "x";
            } else {
                cout << setw(4) << (int) x[i][j];
            }
        }
        cout << "\n";
    }
}

int main(int argc, char *argv[]) {

    int w, h,
            x, y,
            bombs, bombsSet,
            keya, keyX, keyY;

    unsigned char key;
    bool gameOver = false;

    cout << "W: ";
    cin >> w;
    cout << "H: ";
    cin >> h;
    cout << "Bombs: ";
    cin >> bombs;

    srand(time(NULL));

    int **bombsArr = new int*[w];
    int **playSheet = new int*[w];

    for (int i = 0; i < w; ++i) {
        bombsArr[i] = new int[h];
        playSheet[i] = new int[h];
    }

    bombsSet = 0;

    for (int i = 0, max = w; i < max; i++) {
        for (int j = 0, maxJ = h; j < maxJ; j++) {
            bombsArr[i][j] = 0;
            playSheet[i][j] = 0;
        }
    }

    while (bombsSet < bombs) {
        int i = rand() % w;
        int j = rand() % h;
        bombsArr[i][j] = 9;
        bombsSet++;
    }

    printSheet(playSheet, w, h);

    keyX = keyY = 0;


    while (!gameOver) {
        cout << key << endl;
        cout << "Y X: ";
        cin >> x >> y;

        key = getch();
        cout << key << "\n";

        switch (key) {
            case KEY_UP:
                y = keyY - 1;
                break;
            case KEY_DOWN:
                y = keyY + 1;
                break;
            case KEY_LEFT:
                x = keyX - 1;
                break;
            case KEY_RIGHT:
                x = keyX + 1;
                break;
        }

        cout << x << " " << y << "\n";
        if (bombsArr[x][y] != 9) {
            int cBombs = 0;
            /*if( bombsArr[x-1][y-1] != 9 ) { bombsArr[x-1][y-1] += 1; }
            if( bombsArr[x-1][y] != 9 ) { bombsArr[x-1][y] += 1; }
            if( bombsArr[x-1][y+1] != 9 ) { bombsArr[x-1][y+1] += 1; }
            if( bombsArr[x][y-1] != 9 ) { bombsArr[x][y-1] += 1; }
            if( bombsArr[x][y+1] != 9 ) { bombsArr[x][y+1] += 1; }
            if( bombsArr[x+1][y-1] != 9 ) { bombsArr[x+1][y-1] += 1; }
            if( bombsArr[x+1][y] != 9 ) { bombsArr[x+1][y] += 1; }
            if( bombsArr[x+1][y+1] != 9 ) { bombsArr[x+1][y+1] += 1; }*/
            if (x - 1 != -1) {
                if (bombsArr[x - 1][y - 1] == 9) {
                    ++cBombs;
                }
                if (bombsArr[x - 1][y] == 9) {
                    ++cBombs;
                }
                if (bombsArr[x - 1][y + 1] == 9) {
                    ++cBombs;
                }
            }
            if (bombsArr[x][y - 1] == 9) {
                ++cBombs;
            }
            if (bombsArr[x][y + 1] == 9) {
                ++cBombs;
            }
            if (x + 1 != w) {
                if (bombsArr[x + 1][y - 1] == 9) {
                    ++cBombs;
                }
                if (bombsArr[x + 1][y] == 9) {
                    ++cBombs;
                }
                if (bombsArr[x + 1][y + 1] == 9) {
                    ++cBombs;
                }
            }
            playSheet[x][y] = cBombs;
            bombsArr[x][y] = -1;

            for (int i = 0, max = w; i < max; i++) {
                for (int j = 0, maxJ = h; j < maxJ; j++) {
                    if (bombsArr[i][j] != -1 || bombsArr[i][j] != 9) {
                        break;
                    }
                    cout << "You are the man! \n";
                    gameOver = true;
                }
            }
            printSheet(playSheet, w, h);
        } else {
            cout << "Game Over dude! \n";
            printDebug(bombsArr, w, h);
            gameOver = true;
        }
    }
    //printDebug( playSheet, w, h);	
    //printDebug( bombsArr, w, h);    

    system("PAUSE");
    return EXIT_SUCCESS;
}